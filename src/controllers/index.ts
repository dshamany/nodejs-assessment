import { Request, Response } from "express";

export const index = (req: Request ,res: Response) => {
	res.set('Content-Type', 'text/html');
	res.status(200).render('index', { title: 'Add User' })
}