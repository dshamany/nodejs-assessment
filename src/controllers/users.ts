import fs from 'fs';
import path from 'path';
import { Request, Response } from 'express';
import { User } from '../types/user';

const usersFilePath = path.join(__dirname, '../data/users.json');

export const getUsers = () => JSON.parse(fs.readFileSync(usersFilePath, 'utf8'));
export const saveUsers = (users: User[]) => fs.writeFileSync(usersFilePath, JSON.stringify(users, null, 2));

export const getAllUsers = async (req: Request, res: Response) => {
	const users: User[] = await getUsers();
	res.json(users);
};

export const createUser = async (req: Request, res: any) => {
	res.set("Access-Control-Allow-Origin", "http://localhost:3000");
	const users: User[] = getUsers();
	const newUser: User = {id: users.length + 1, ...req.body};
	console.log(newUser);
	users.push(newUser);
	await saveUsers(users);
	res.status(201).json(newUser);
};

export const getUserById = async (req: Request, res: Response) => {
	const users = await getUsers();
	const userId: Number = parseInt(req.params.id);
	let user: User | undefined;
	
	for (let i = 0; i < users.length; i++) {
		if (users[i].id === userId) {
			user = users[i];
			break;
		}
	}
	
	if (user) {
		res.json(user);
	} else {
		res.status(404).json({error: 'User not found'});
	}
};

export const updateUser = async (req: Request, res: Response) => {
	let users: User[] = getUsers();
	const userId: Number = parseInt(req.params.id);
	const {name, email, address} = req.body;
	let user: User | undefined;
	
	for (let i = 0; i < users.length; i++) {
		if (users[i].id === userId) {
			user = users[i];
			break;
		}
	}
	
	if (user) {

		// Ensure that updates can be made to individual attributes
		const updatedUser: User = {
			...user,
			name: name || user.name,
			email: email || user.email,
			address: address || user.address
		};

		// Replace user with updated user in the array
		users.splice(users.indexOf(user), 1, updatedUser);

		await saveUsers(users);
		
		res.status(201).json(user);
	} else {
		res.status(404).json({error: 'User not found'});
	}
};

export const deleteUser = async (req: Request, res: Response) => {
	const users = getUsers();
	const userId = parseInt(req.params.id);
	let user: User | undefined;
	
	for (let i = 0; i < users.length; i++) {
		if (users[i].id === userId) {
			user = users[i];
			break;
		}
	}
	
	if (user) {
		const updatedUsers = users.filter((u: User) => u.id !== userId);
		await saveUsers(updatedUsers);
		res.json({message: 'User deleted successfully'});
	} else {
		res.status(404).json({error: 'User not found'});
	}
};

export default {
	getAllUsers,
	createUser,
	getUserById,
	updateUser,
	deleteUser
}