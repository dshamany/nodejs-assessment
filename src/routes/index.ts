import * as indexController from '../controllers';
import { Router } from 'express';

const router = Router();

router.get('/', indexController.index);

export default router;