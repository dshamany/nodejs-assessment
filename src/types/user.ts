export interface Address {
    street: string;
    city: string;
    state: string;
    zip: string;
};

export interface User {
    id: number;
    name: string;
    email: string;
    address: Address;
};