import express from 'express';
import path from 'path';
import dotenv from 'dotenv';

const cors = require('cors');
const morgan = require('morgan');

dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());
app.use(morgan('dev'));

// Add ejs to render views
app.set('view engine', 'ejs');
// Ensure you use the views folder
app.set('views', path.join(__dirname, '/views'));

// Allow cors to ease API usage for this demo

// Import routes below app.use(express.json()) so that the 
// body is parsed correctly
import * as indexRoutes from './routes/index';
import * as userRoutes from './routes/users';

// Attach routes to app
app.use('/', indexRoutes.default);
app.use('/users', userRoutes.default);

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
