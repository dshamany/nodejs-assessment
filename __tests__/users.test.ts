/*
    I am including a few tests here to demonstrate how a mock test
    would be implemented. 

    This is not exhaustive by any stretch of imagination.
*/


import fs from "fs";
import { Request, Response } from "express";
import { describe, expect, it } from "@jest/globals";

import { getAllUsers, getUserById, updateUser } from "../src/controllers/users";
import { User } from "../src/types/user";

// Mock Data
const mockUsers: User[] = JSON.parse(
  fs.readFileSync("./src/data/users.json", "utf8")
);

describe("Users API Tests", () => {
  it("mockUsers are valid", async () => {
    expect(mockUsers.length).toBeGreaterThan(0);
  });

  it("getAllUsers fetches users from the users.json file", async () => {
    const req: Request = {} as Request;
    const res: Response = {
      json: jest.fn(),
    } as unknown as Response;

    await getAllUsers(req, res);

    expect(res.json).toHaveBeenCalledWith(mockUsers);
  });

  it("get user by ID", async () => {
    let req: Request = {
      params: { id: '1' },
    } as unknown as Request;
    const res: Response = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    } as unknown as Response;

    await getUserById(req, res);

    expect(res.json).toHaveBeenCalledWith(mockUsers[0]);
  });

  it("should return a 404 if the user is not found", async () => {
    const failedUserId = 10;
    const req = {
      params: { id: failedUserId.toString() },
    } as unknown as Request;

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    } as unknown as Response;

    await getUserById(req, res);

    // expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({ error: "User not found" });
  });
});
