module.exports = {
	testEnvironment: 'node',
	coveragePathIgnorePatterns: ['/node_modules/'],
	modulePathIgnorePatterns: ['<rootDir>/dist/'],
	preset: "ts-jest",
	testMatch: ["**/*.test.ts"],
	transform: {
		"^.+\\.tsx?$": "ts-jest"
	}
};
